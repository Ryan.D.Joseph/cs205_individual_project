import sys
if sys.version_info.major < 3 or sys.version_info.minor < 7:
    print(f"This program requires Python 3.7 or greater. You are currently on {sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}")
    print("Please upgrade and run again. Thank you.")
    sys.exit(-1)


import datetime 
import Review
import Book
import Series
import User
import ReviewSystem

def main():
    review_system = ReviewSystem.ReviewSystem()
    print("Welcome to the Library!")
    nick = input("Please enter your username: ")
    email = input("Please enter your email: ")
    user = User.User(nick,email)
    
    print("[A]dd Book to System; [S]tart Reading Book; [F]inish Reading Book; [R]eview Book; [Q]uit")
    action = input("> ")
    while action != "Q":
        if action == "A":
            title  = input("Series Title: ")
            author = input("Series Author: ")
            series = Series.Series(title,author)
            another_book = "Y"
            while another_book != "N":
                book_title = input("Book Title: ")
                series.addBook(Book.Book(book_title,author))
                another_book = input("Another? [Y/N]")
                
            review_system.addContent(series)
        elif action == "S":
            title = input("What series would you like to read? ")
            series = review_system.getSeriesByTitle(title)
            while series == None:
                print("Sorry, we don't have that series. Try again")
                title = input("What series would you like to read? ")
                series = review_system.getSeriesByTitle(title)

            title = input("What book? ")
            book = series.getBookByName(title)
            while book == None:
                print("Sorry, we don't have that title. Try again")
                title = input(f"What book of {series.getTitle()} ")
                book = series.getBookByName(title)

            user.startReading(book)
        elif action == "F":
            title = input("What book have you finished?")
            reading = user.getReading()
            book = next(filter(lambda x: (x.getTitle() == title),reading),None)
            while book == None and title != "Q":
                print("It seems like you weren't reading that book.")
                print("Retry, or enter Q to go back to the top level")
                title = input("What book have you finished?")
                book = next(filter(lambda x: (x.getTitle() == title),title),None)
                
            if title == "Q":
                break
            else:
                user.markAsRead(book)
        elif action == "R":
            title = input("What book would you like to review?")
            reading = user.getReadLogWithDelta()
            book_tuple = next(filter(lambda x: (x[0].getTitle() == title),reading),None)
            while book_tuple == None and title != "Q":
                print("It seems like you haven't read that book. We do not allow reviews of books you haven't read.")
                print("Retry, or enter Q to go back to the top level")
                title = input("What book would you like to review?")
                book_tuple = next(filter(lambda x: (x[0].getTitle() == title),reading),None)
                
            if title == "Q":
                break
            else:
                print("Type your review all on one line. Hit enter to finish. Brevity is the soul of wit")
                review_content = input("> ")
                rating = int(input("And you overall rating? [1-10]: "))
                time = book_tuple[1]
                review = Review.Review(user,time,rating,review_content)
                book_tuple[0].addReview(review)
        elif action == "Q":
            pass
        else:
            print("Sorry we didn't understand that.")

        print("[A]dd Book to System; [S]tart Reading Book; [F]inish Reading Book; [R]eview Book; [Q]uit")
        action = input("> ")


    
main()
