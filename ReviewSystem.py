class ReviewSystem():
    def __init__(self):
        self.users = [] 
        self.content = []

    def addContent(self,content):
        self.content.append(content)

    def addUser(self,user):
        self.users.append(user)

    def getUsers(self):
        return self.users

    def getUserByName(self,name):
        return next((filter(lambda x: (x.getName() == name),self.users)),None)

    def getSeriesByTitle(self,title):
        return next((filter(lambda x: (x.getTitle() == title),self.content)),None)

    def getSeriesByAuthor(self,author):
        return list((filter(lambda x: (x.getAuthor() == author),self.content)))
