
class Book():
    def __init__(self,title,author,reviews=[]):
        self.title = title
        self.author = author
        self.reviews = reviews

    def addReview(self,review):
        self.reviews.append(review)

    def getTitle(self):
        return self.title 

    def getAuthor(self):
        return self.author
    
    def getReviews(self):
        return self.reviews 

    def getReviewsByUser(user):
        return list(filter(lambda x: (x.getUser() == user), self.reviews))

    def getAverageRating(self):
        ratings = list(map(lambda x: x.getRating(),self.reviews))
        return (sum(ratings) // len(ratings))

    def __eq__(self,other):
        if (isinstance(other,Book)):
            return self.author == other.author and self.title == other.title
        return False

    # The functions python's print calls. Implementing it like this means I can call print on a series or a book object equally, allowing python to do the runtime type-reflection 
    def __str__(self):
        return f"{self.title}, by {self.author}"

    def __repr__(self):
        return f"{self.title}, by {self.author}"
