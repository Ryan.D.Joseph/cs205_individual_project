class Review():
    def __init__(self,user,reading_time,rating,content):
        self.user = user
        self.reading_time = reading_time
        self.rating = rating
        self.content = content

    def getRating(self):
        return self.rating

    def getUser(self):
        return self.user 

    def getReviewContent(self):
        return self.content 

    def printReview(self):
        print(f"From: {self.user.getName()}")
        print(f"Rating: {self.rating}")
        print(self.content)
        print(f"Forward responses to: {self.user.getEmail()}")

    
