# This has to come first, otherwise Python tries to autoload the dataclass module and it just crashes. This at least provides a nice error message and straightforward guide.
# Also, to the grader: I apologize if you have to upgrade Python just to grade this assignment. I realized only after I had written the program using dataclasses that they were a >3.7 feature. 
import sys
if sys.version_info.major < 3 or sys.version_info.minor < 7:
    print(f"This program requires Python 3.7 or greater. You are currently on {sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}")
    print("Please upgrade and run again. Thank you.")
    sys.exit(-1)

import unittest

import datetime 
import Review
import Book
import Series
import User
import ReviewSystem

class TestReviewSystem(unittest.TestCase):
    review_system = None 

    # It makes testing easier to just totally rebuild the world between every test, so I don't have to come up with new books. The use-case for this system never actually deletes content, so it doesn't make sense to add functions to reset things just for testing purposes
    @classmethod
    def setUpClass(cls):
        cls.review_system = ReviewSystem.ReviewSystem()
        
    @classmethod
    def setUp(self):
        # Create some books
        book_1 = Book.Book("The Fifth Season","N.K. Jemisin")
        book_2 = Book.Book("The Obelisk Gate","N.K. Jemisin")
        book_3 = Book.Book("The Stone Sky","N.K. Jemisin")

        book_4 = Book.Book("The Long Way To A Small Angry Planet","Becky Chambers")
        book_5 = Book.Book("A Closed And Common Orbit","Becky Chambers")
        book_6 = Book.Book("Record of a Spaceborn Few","Becky Chambers")

        book_7 = Book.Book("A Deadly Education","Naomi Novik")
        
        # Create some series
        series_1 = Series.Series("The Broken Earth","N.K. Jemisin",[book_1,book_2,book_3])
        series_2 = Series.Series("Wayfarer","Becky Chambers",[book_4,book_5,book_6])
        series_3 = Series.Series("The Scholomance","Naomi Novik",[book_7])
        
        self.review_system.addContent(series_1)
        self.review_system.addContent(series_2)
        self.review_system.addContent(series_3)

        # Create a User
        user = User.User("rdjoseph@uvm.edu","RDJ")

        self.review_system.addUser(user)
        
    @classmethod
    def tearDown(self):
        self.review_system = ReviewSystem.ReviewSystem() # Just totally reset everything, let the garbage collector figure things out 
    
    def test_find_series_by_name(self):
        """ Find a series that should be in the system by it's name. Passes if Series object returned """ 
        TBE = self.review_system.getSeriesByTitle("The Broken Earth")
        self.assertTrue(TBE != None)

    def test_find_series_by_author(self):
        """ Find a series that should be in the system. Passes if nonempty Series list returned """ 
        WFS = self.review_system.getSeriesByAuthor("Becky Chambers")
        self.assertTrue(len(WFS) != 0)

    def test_user_start_reading(self):
        """ For a user in the system, put a book on their currently reading list. Passes if User.isReading returns true afterwards """
        user = self.review_system.getUserByName("RDJ")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        book = series.getBookByIndex(0)

        user.startReading(book)
        self.assertTrue(user.isReading(book))

    def test_user_stop_reading(self):
        """ For a user in the system, mark a book on their reading list as done. Passes if User.isReading/.hasRead return the False & True respectively after """ 
        user = self.review_system.getUserByName("RDJ")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        book = series.getBookByIndex(0)

        user.startReading(book,datetime.datetime(2021,1,1))
        user.markAsRead(book,datetime.datetime(2021,3,5))
        self.assertFalse(user.isReading(book))
        self.assertTrue(user.hasRead(book))

    def test_user_stop_reading_incorrect(self):
        """ Test for incorrect implementation of User.markAsRead. The isReading(book) assertion should fail, as the incorrect implementation puts the book on the read list but does not remove it from the reading list """ 
        user = self.review_system.getUserByName("RDJ")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        book = series.getBookByIndex(0)

        user.startReading(book,datetime.datetime(2021,1,1))
        user.wrong_markAsRead(book,datetime.datetime(2021,3,5))
        self.assertFalse(user.isReading(book)) # Test should fail here 
        self.assertTrue(user.hasRead(book))

    def test_review_series(self):
        """ Test to add a review to a series in the system. Passes if Series returns a populated list of reviews and the correct average rating """ 
        user = self.review_system.getUserByName("RDJ")
        review = Review.Review(user,datetime.timedelta(days=10),10,"A stunning exploration of high fantasy and social issues")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        series.addReview(review)
        self.assertTrue(len(series.getReviews()) != 0)
        self.assertTrue(series.getAverageRating() == 10)

    def test_review_book(self):
        """ Test to add a review to a book in the system. Passes if Book returns a populated list of reviews and the correct average rating """ 
        user = self.review_system.getUserByName("RDJ")
        review = Review.Review(user,datetime.timedelta(days=10),10,"An amazing debut of a fraut fantasy world with all-too-real societal issues")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        book = series.getBookByName("The Fifth Season")
        book.addReview(review)
        self.assertTrue(len(book.getReviews()) != 0)
        self.assertTrue(book.getAverageRating() == 10)

    def test_user_readlog(self):
        """ Test the user's readlog function. Passes if a populated list is returned """
        user = self.review_system.getUserByName("RDJ")
        series = self.review_system.getSeriesByTitle("The Broken Earth")
        book = series.getBookByIndex(0)

        user.startReading(book,datetime.datetime(2021,1,1))
        user.markAsRead(book,datetime.datetime(2021,3,5))
        read = user.getReadLog()
        self.assertTrue(len(read) != 0)



if __name__ == '__main__':
    unittest.main()
