import datetime
import Book
from dataclasses import dataclass

@dataclass
class BookData:
    start_date: datetime.datetime
    book: Book
    end_date: datetime.datetime
    
class User():
    def __init__(self,email,name):
        self.email = email
        self.name = name
        self.reading = [] #A list of (datetime,Book)
        self.read = []      #A list of (datetime,Book,datetime) 

    def getEmail(self):
        return self.email 

    def getName(self):
        return self.name 

    def getReading(self):
        return [book_data.book for book_data in self.reading] # Books are stored with their dates, we only want the books

    def startReading(self,book,time=datetime.datetime.now()):
        log_item = BookData(time,book,None)
        self.reading.append(log_item)

    def isReading(self,book):
        for reading_book in self.reading:
            if reading_book.book == book:
                return True
            
        return False

    def hasRead(self,book):
        for read_book in self.read:
            if read_book.book == book:
                return True
            
        return False

    
    def wrong_markAsRead(self,book,time=datetime.datetime.now()):
        """ A purposefully wrong implementation of markAsRead, that fails to remove the book from the current reading list """ 
        read_book = next((x for x in self.reading if x.book == book),None)
        time = datetime.datetime.now()

        if read_book == None: # the case a book goes straight into the read pile
            read_book = BookData(time,book,time)
        else:
            read_book.end_date = time

        self.read.append(read_book)


    def markAsRead(self,book,time=datetime.datetime.now()):
        """ Move the book from the reading to read pile, and add the time data to mark when it was finished"""
        read_book = next((x for x in self.reading if x.book == book),None)
        
        time = datetime.datetime.now()
        
        if read_book == None: # In the case a book goes straight into the read pile
            read_book = BookData(time,book,time)
        else:
            read_book.end_date = time
            self.reading.remove(read_book) # This line was added to fix the error in wrong_markAsRead. Now, the class correctly removes the book from the current reading list and adds it to the read list

        self.read.append(read_book)
        
    def getReadLog(self,start=datetime.datetime.min,end=datetime.datetime.max):
        read_in_period = list(filter(lambda x: (x.start_date >= start and x.end_date <= end),self.read)) # Get all the books started between those two dates
        read_in_period.sort(key=lambda x: x.start_date) # Sort by started date. Sort runs in-place, for some awful reason, so it has to be called on the filter result
        read_books_in_period = [book_data.book for book_data in read_in_period] # Books are stored with their dates, we only want the books
        return read_books_in_period

    def getReadLogWithDelta(self,start=datetime.datetime.min,end=datetime.datetime.max):
        read_in_period = list(filter(lambda x: (x.start_date >= start and x.end_date <= end),self.read)) # Get all the books started between those two dates
        read_in_period.sort(key=lambda x: x.start_date) # Sort by started date. Sort runs in-place, for some awful reason, so it has to be called on the filter result
        read_books_in_period = [(book_data.book, book_data.end_date - book_data.start_date) for book_data in read_in_period] # Books are stored with their dates, we only want the books
        return read_books_in_period

    def __eq__(self,other):
        if (isinstance(other,User)):
            return self.name == other.name and self.email == other.email
        return False


        
