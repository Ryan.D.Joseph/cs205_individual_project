# Individual Project 

For my individual project, I wrote a small system to keep track of books/series I've read, and the reviews I write for them. 

# Note On Python Version Requirements

Because I used dataclasses as a component in my Book class, you will need Python >3.7 to run this project.
