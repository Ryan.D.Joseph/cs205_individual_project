class Series():
    def __init__(self,title,author,books=[],reviews=[]):
        self.title = title
        self.author = author
        self.books = books
        self.reviews = reviews

    def addBook(self,book):
        """ Add book to series """
        self.books.append(book)

    def addBooks(self,books):
        """ Add list of books to series """
        self.books += books

    def getBookByName(self,book_title):
        """ Return the book in the series corresponding to the provided title """ 
        return next((filter(lambda x: (x.getTitle() == book_title),self.books)),None)
    
    def getBookByIndex(self,book_index):
        """ Return book by placement in series (ie book 1 is 0, book 2 is 1, and so on). Assumes books were inserted into series in the correct order """
        if book_index >= len(self.books):
            return None
        else:
            return self.books[book_index]
        
    def getAuthor(self):
        return self.author

    def getTitle(self):
        return self.title

    def addReview(self,review):
        self.reviews.append(review)

    def getReviews(self):
        return self.reviews

    def getAverageRating(self):
        ratings = list(map(lambda x: x.getRating(),self.reviews))
        return (sum(ratings) // len(ratings))

    # The functions python's print calls. Implementing it like this means I can call print on a series or a book object equally, allowing python to do the runtime type-reflection 
    def __str__(self):
        series_title = f"Series '{self.title}', by {self.author}:"
        for book in self.books:
            series_title += "\n\t"
            series_title += book.__str__
        return series_title

    def __repr__(self):
        series_title = f"Series '{self.title}', by {self.author}:"
        for book in self.books:
            series_title += "\n\t"
            series_title += book.__str__
        return series_title



    
